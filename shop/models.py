from __future__ import unicode_literals

from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=30)
    slug = models.CharField(max_length=30, default='')

    def __unicode__(self):
        return self.name


class Item(models.Model):
    title = models.CharField(max_length=50)
    slug = models.CharField(max_length=50, default='')
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    category = models.ForeignKey(Category, related_name='items', blank=True, null=True)
    image = models.ImageField(upload_to='items', blank=True, null=True)

    def __unicode__(self):
        return self.title


class Slider(models.Model):
    image = models.ImageField(upload_to='slider')
    title = models.CharField(max_length=50)
    description = models.TextField()

    def __unicode__(self):
        return self.title
